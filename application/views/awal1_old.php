<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="API Ketersediaan Bed">
  <meta name="author" content="Dinas Kesehatan Provinsi DKI Jakarta">

  <title>API KETERSEDIAAN BED RUMAH SAKIT PROVINSI DKI JAKARTA</title>

  <link rel="icon" type="image/ico" sizes="16x16" href="<?php echo base_url();?>assets/favicon.ico">
  <!-- Bootstrap core CSS -->
  <link href="<?php echo base_url();?>assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom fonts for this template -->
  <link href="https://fonts.googleapis.com/css?family=Saira+Extra+Condensed:500,700" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Muli:400,400i,800,800i" rel="stylesheet">
  <link href="<?php echo base_url();?>assets/vendor/fontawesome-free/css/all.min.css" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="<?php echo base_url();?>assets/css/resume.min.css" rel="stylesheet">
  <link href="<?php echo base_url();?>assets/css/custom.css" rel="stylesheet">

</head>

<body id="page-top">

  <nav class="navbar navbar-expand-lg navbar-dark bg-primary fixed-top" id="sideNav">
    <a class="navbar-brand js-scroll-trigger" href="#page-top">
      <!-- <span class="d-block d-lg-none">Clarence Taylor</span>
      <span class="d-none d-lg-block">
        <img class="img-fluid img-profile rounded-circle mx-auto mb-2" src="<?php echo base_url();?>assets/img/profile.jpg" alt="">
      </span> -->
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link js-scroll-trigger" href="#home">Home</a>
        </li>
        <li class="nav-item">
          <a class="nav-link js-scroll-trigger" href="#header">Header</a>
        </li>
        <li class="nav-item">
          <a class="nav-link js-scroll-trigger" href="#service">Service</a>
        </li>
      </ul>
    </div>
  </nav>

  <div class="container-fluid p-0">

    <section class="resume-section p-3 p-lg-5 d-flex align-items-center" id="home">
      <div class="w-100">
        <div class="subheading mb-5">
          Selamat Datang!.
        </div>
        <p class="lead mb-5">
          Katalog API (Application programming interface) Ketersediaan Bed Rumah Sakit Provinsi DKI Jakarta.
        </p>
        <p class="lead mb-5" style="font-size: 13px">
          © 2020 Dinas Kesehatan Provinsi DKI Jakarta. <br>Developed by Seksi Data Informasi dan Hubungan Masyarakat.
        </p>
      </div>
    </section>

    <hr class="m-0">

    <section class="resume-section p-3 p-lg-5 d-flex justify-content-center" id="header">
      <div class="w-100">
        <h2 class="mb-5">Header</h2>

        <div class="resume-item d-flex flex-column flex-md-row justify-content-between mb-5">
          <div class="resume-content">
            <p>Secara umum, hampir setiap pemanggilan web-service, harus dicantumkan beberapa variabel yang dibutuhkan untuk menambahkan informasi ataupun untuk proses validasi yang dikirim pada HTTP Header, antara lain:</p>
          </div>
        </div>

        <div class="resume-item d-flex flex-column flex-md-row justify-content-between mb-5">
          <div class="resume-content">
            <table cellpadding="1" cellspacing="1" border="1px" style="border-collapse: collapse; line-height:20px; padding:5px; margin-top:10px;">
          <tr style="background-color:#18181d; height:40px; text-align:center; color: white;">
            <th style="width: 190px !important">Nama Header</th>
            <th>Contoh Nilai</th>
            <th>Keterangan</th>
          </tr>
          <tr style="height:40px;">
            <td>Api-Bed-User</td>
            <td>317509</td>
            <td>Kode Rumah Sakit yang diberikan oleh Dinas Kesehatan Provinsi DKI Jakarta</td>
          </tr>
          <tr style="height:40px;">
            <td>Api-Bed-Key</td>
            <td>$2y$10$wnudBW3apv9ECayoiML0ouTL.ZHrhDjJ576f</td>
            <td>Unique Key yang diberikan oleh Dinas Kesehatan Provinsi DKI Jakarta untuk masing-masing Rumah Sakit</td>
          </tr>
        </table>
          </div>
        </div>

        <div class="resume-item d-flex flex-column flex-md-row justify-content-between mb-5">
          <div class="resume-content">
            <h3 class="mb-0">Api-Bed-User</h3>
            <p>Merupakan kode Rumah Sakit (pengakses web-service). Kode ini akan diberikan oleh Dinas Kesehatan Provinsi DKI Jakarta.</p>
          </div>
        </div>

        <div class="resume-item d-flex flex-column flex-md-row justify-content-between mb-5">
          <div class="resume-content">
            <h3 class="mb-0">Api-Bed-Key</h3>
            <p>Merupakan kode otentifikasi unik (pengakses web-service). Kode ini akan diberikan oleh Dinas Kesehatan Provinsi DKI Jakarta.</p>
          </div>
        </div>
      </div>
    </section>

    <hr class="m-0">

    <section class="resume-section p-3 p-lg-5 d-flex align-items-center" id="service">
      <div class="w-100">
        <h2 class="mb-5">Service V1</h2>

        <div class="resume-item d-flex flex-column flex-md-row justify-content-between mb-5">
          <div class="resume-content">
            <h3 class="mb-0">Kirim Data Ketersediaan Bed</h3>
            <div class="subheading mb-3">Post Ketersediaan Bed (Method : POST)</div>
            <div>URL : {BASE_URL}/bed</div>
            <p>&nbsp;</p>
            <p>Body</p>
            <pre class="brush: ruby" name="code">
              {
                    "kapasitas_vip": "5",
                    "kapasitas_kelas_1": "5",
                    "kapasitas_kelas_2": "0",
                    "kapasitas_kelas_3": "0",
                    "kapasitas_icu": "0",
                    "kapasitas_nicu": "0",
                    "kapasitas_picu": "0",
                    "kapasitas_hcu": "0",
                    "kapasitas_iccu": "0",
                    "kapasitas_isolasi": "0",
                    "kapasitas_icu_covid": "0",
                    "kapasitas_isolasi_covid": "0",
                    "kosong_vip": "3",
                    "kosong_kelas_1": "89",
                    "kosong_kelas_2": "91",
                    "kosong_kelas_3": "0",
                    "kosong_icu": "0",
                    "kosong_nicu": "0",
                    "kosong_picu": "0",
                    "kosong_hcu": "0",
                    "kosong_iccu": "0",
                    "kosong_isolasi": "0",
                    "kosong_icu_covid": "0",
                    "kosong_isolasi_covid": "0"
              }
            </pre>
            <p>Keterangan</p>
            <div>kapasitas_vip = Informasi Kapasitas Bed VIP</div>
            <div>kapasitas_kelas_1 = Informasi Kapasitas Bed Kelas 1</div>
            <div>kapasitas_kelas_2 = Informasi Kapasitas Bed Kelas 2</div>
            <div>kapasitas_kelas_3 = Informasi Kapasitas Bed Kelas 3</div>
            <div>kapasitas_icu = Informasi Kapasitas Bed ICU</div>
            <div>kapasitas_nicu = Informasi Kapasitas Bed NICU</div>
            <div>kapasitas_picu = Informasi Kapasitas Bed PICU</div>
            <div>kapasitas_hcu = Informasi Kapasitas Bed HCU</div>
            <div>kapasitas_iccu = Informasi Kapasitas Bed ICCU</div>
            <div>kapasitas_isolasi = Informasi Kapasitas Bed Isolasi</div>
            <div>kapasitas_icu_covid = Informasi Kapasitas Bed ICU Khusus Penanganan COVID-19</div>
            <div>kapasitas_isolasi_covid = Informasi Kapasitas Bed Isolasi Khusus Penanganan COVID-19</div>

            <div>kosong_vip = Informasi Jumlah Bed VIP yang tersedia</div>
            <div>kosong_kelas_1 = Informasi Jumlah Bed Kelas 1 yang tersedia</div>
            <div>kosong_kelas_2 = Informasi Jumlah Bed Kelas 2 yang tersedia</div>
            <div>kosong_kelas_3 = Informasi Jumlah Bed Kelas 3 yang tersedia</div>
            <div>kosong_icu = Informasi Jumlah Bed ICU yang tersedia</div>
            <div>kosong_nicu = Informasi Jumlah Bed NICU yang tersedia</div>
            <div>kosong_picu = Informasi Jumlah Bed PICU yang tersedia</div>
            <div>kosong_hcu = Informasi Jumlah Bed HCU yang tersedia</div>
            <div>kosong_iccu = Informasi Jumlah Bed ICCU yang tersedia</div>
            <div>kosong_isolasi = Informasi Jumlah Bed Isolasi yang tersedia</div>
            <div>kosong_icu_covid = Informasi Jumlah Bed ICU Khusus Penanganan COVID-19 yang tersedia</div>
            <div>kosong_isolasi_covid = Informasi Jumlah Bed Isolasi Khusus Penanganan COVID-19 yang tersedia</div>

            <p>&nbsp;</p>
            <p>Response Success</p>
            <pre class="brush: ruby" name="code">
              {
                  "kode": 200,
                  "messages": "SUCCESS POST DATA BED"
              }
            </pre>
          </div>
        </div>

        <div class="resume-item d-flex flex-column flex-md-row justify-content-between mb-5">
          <div class="resume-content">
            <h3 class="mb-0">Lihat Data Ketersediaan Bed</h3>
            <div class="subheading mb-3">Get Ketersediaan Bed (Method : GET)</div>
            <div>URL    : {BASE_URL}/bed</div>
            <p>&nbsp;</p>
            <p>Response Success</p>
            <pre class="brush: ruby" name="code">
              {
                  "kode": 200,
                  "messages": "SUCCESS GET DATA BED",
                  "response": {
                      "faskes": "RSUPN Dr. Cipto Mangunkusumo",
                      "kapasitas_vip": "5",
                      "kapasitas_kelas_1": "5",
                      "kapasitas_kelas_2": "0",
                      "kapasitas_kelas_3": "0",
                      "kapasitas_icu": "0",
                      "kapasitas_nicu": "0",
                      "kapasitas_picu": "0",
                      "kapasitas_hcu": "0",
                      "kapasitas_iccu": "0",
                      "kapasitas_isolasi": "0",
                      "kapasitas_icu_covid": "0",
                      "kapasitas_isolasi_covid": "0",
                      "kosong_vip": "3",
                      "kosong_kelas_1": "89",
                      "kosong_kelas_2": "91",
                      "kosong_kelas_3": "0",
                      "kosong_icu": "0",
                      "kosong_nicu": "0",
                      "kosong_picu": "0",
                      "kosong_hcu": "0",
                      "kosong_iccu": "0",
                      "kosong_isolasi": "0",
                      "kosong_icu_covid": "0",
                      "kosong_isolasi_covid": "0",
                      "updated_time": "2020-09-02 14:57:07"
                  }
              }
            </pre>
          </div>
        </div>

        <div class="resume-item d-flex flex-column flex-md-row justify-content-between mb-5">
          <div class="resume-content">
            <h3 class="mb-0">Hapus Data Ketersediaan Bed</h3>
            <div class="subheading mb-3">Delete Ketersediaan Bed (Method : DELETE)</div>
            <div>URL    : {BASE_URL}/bed</div>
            <p>&nbsp;</p>
            <p>Response</p>
            <pre class="brush: ruby" name="code">
              {
                  "kode": 200,
                  "messages": "SUCCESS DELETE DATA BED"
              }
            </pre>
          </div>
        </div>

        
      </div>
    </section>

    <hr class="m-0">

  </div>

  <!-- Bootstrap core JavaScript -->
  <script src="<?php echo base_url();?>assets/vendor/jquery/jquery.min.js"></script>
  <script src="<?php echo base_url();?>assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Plugin JavaScript -->
  <script src="<?php echo base_url();?>assets/vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for this template -->
  <script src="<?php echo base_url();?>assets/js/resume.min.js"></script>

</body>

</html>
