<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="API Ketersediaan Bed">
  <meta name="author" content="Dinas Kesehatan Provinsi DKI Jakarta">

  <title>API KETERSEDIAAN BED RUMAH SAKIT PROVINSI DKI JAKARTA</title>

  <link rel="icon" type="image/ico" sizes="16x16" href="<?php echo base_url();?>assets/favicon.ico">
  <!-- Bootstrap core CSS -->
  <link href="<?php echo base_url();?>assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom fonts for this template -->
  <link href="https://fonts.googleapis.com/css?family=Saira+Extra+Condensed:500,700" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Muli:400,400i,800,800i" rel="stylesheet">
  <link href="<?php echo base_url();?>assets/vendor/fontawesome-free/css/all.min.css" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="<?php echo base_url();?>assets/css/resume.min.css" rel="stylesheet">
  <link href="<?php echo base_url();?>assets/css/custom.css" rel="stylesheet">

</head>

<body id="page-top">

  <nav class="navbar navbar-expand-lg navbar-dark bg-primary fixed-top" id="sideNav">
    <a class="navbar-brand js-scroll-trigger" href="#page-top">
      <!-- <span class="d-block d-lg-none">Clarence Taylor</span>
      <span class="d-none d-lg-block">
        <img class="img-fluid img-profile rounded-circle mx-auto mb-2" src="<?php echo base_url();?>assets/img/profile.jpg" alt="">
      </span> -->
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link js-scroll-trigger" href="#home">Home</a>
        </li>
        <li class="nav-item">
          <a class="nav-link js-scroll-trigger" href="#header">Header</a>
        </li>
        <li class="nav-item">
          <a class="nav-link js-scroll-trigger" href="#service">Service</a>
        </li>
      </ul>
    </div>
  </nav>

  <div class="container-fluid p-0">

    <section class="resume-section p-3 p-lg-5 d-flex align-items-center" id="home">
      <div class="w-100">
        <div class="subheading mb-5">
          Selamat Datang!.
        </div>
        <p class="lead mb-5">
          Katalog API (Application programming interface) Ketersediaan Bed Rumah Sakit Provinsi DKI Jakarta.
        </p>
        <p class="lead mb-5" style="font-size: 13px">
          © 2020 Dinas Kesehatan Provinsi DKI Jakarta. <br>Developed by Seksi Data Informasi dan Hubungan Masyarakat.
        </p>
      </div>
    </section>

    <hr class="m-0">

    <section class="resume-section p-3 p-lg-5 d-flex justify-content-center" id="header">
      <div class="w-100">
        <h2 class="mb-5">Header</h2>

        <div class="resume-item d-flex flex-column flex-md-row justify-content-between mb-5">
          <div class="resume-content">
            <p>Secara umum, hampir setiap pemanggilan web-service, harus dicantumkan beberapa variabel yang dibutuhkan untuk menambahkan informasi ataupun untuk proses validasi yang dikirim pada HTTP Header, antara lain:</p>
          </div>
        </div>

        <div class="resume-item d-flex flex-column flex-md-row justify-content-between mb-5">
          <div class="resume-content">
            <table cellpadding="1" cellspacing="1" border="1px" style="border-collapse: collapse; line-height:20px; padding:5px; margin-top:10px;">
          <tr style="background-color:#18181d; height:40px; text-align:center; color: white;">
            <th style="width: 190px !important">Nama Header</th>
            <th>Contoh Nilai</th>
            <th>Keterangan</th>
          </tr>
          <tr style="height:40px;">
            <td>Api-Bed-User</td>
            <td>317509</td>
            <td>Kode Rumah Sakit yang diberikan oleh Dinas Kesehatan Provinsi DKI Jakarta</td>
          </tr>
          <tr style="height:40px;">
            <td>Api-Bed-Key</td>
            <td>$2y$10$wnudBW3apv9ECayoiML0ouTL.ZHrhDjJ576f</td>
            <td>Unique Key yang diberikan oleh Dinas Kesehatan Provinsi DKI Jakarta untuk masing-masing Rumah Sakit</td>
          </tr>
          <tr style="height:40px;">
            <td>Content-Type</td>
            <td>application/json</td>
            <td>Jenis Konten yang dikirimkan Rumah Sakit agar dapat diterima oleh Server Dinas Kesehatan Provinsi DKI Jakarta</td>
          </tr>
        </table>
          </div>
        </div>

        <div class="resume-item d-flex flex-column flex-md-row justify-content-between mb-5">
          <div class="resume-content">
            <h3 class="mb-0">Api-Bed-User</h3>
            <p>Merupakan kode Rumah Sakit (pengakses web-service). Kode ini akan diberikan oleh Dinas Kesehatan Provinsi DKI Jakarta.</p>
          </div>
        </div>

        <div class="resume-item d-flex flex-column flex-md-row justify-content-between mb-5">
          <div class="resume-content">
            <h3 class="mb-0">Api-Bed-Key</h3>
            <p>Merupakan kode otentifikasi unik (pengakses web-service). Kode ini akan diberikan oleh Dinas Kesehatan Provinsi DKI Jakarta.</p>
          </div>
        </div>

        <div class="resume-item d-flex flex-column flex-md-row justify-content-between mb-5">
          <div class="resume-content">
            <h3 class="mb-0">Content-Type</h3>
            <p>Merupakan jenis konten yang dikirimkan oleh Rumah Sakit (pengakses web-service).</p>
          </div>
        </div>
      </div>
    </section>

    <hr class="m-0">

    <section class="resume-section p-3 p-lg-5 d-flex align-items-center" id="service">
      <div class="w-100">
        <h2 class="mb-5">Service V.1.3</h2>

        <div class="resume-item d-flex flex-column flex-md-row justify-content-between mb-5">
          <div class="resume-content">
            <h3 class="mb-0">Base URL</h3>
            <div class="subheading mb-3">http://eis.dinkes.jakarta.go.id/api-bed</div>
          </div>
        </div>

        <div class="resume-item d-flex flex-column flex-md-row justify-content-between mb-5">
          <div class="resume-content">
            <h3 class="mb-0">Post Data Ketersediaan Bed</h3>
            <div class="subheading mb-3">Digunakan untuk mengirimkan data ketersediaan bed di rumah sakit. <br>Untuk keperluan create dan update data , menggunakan method ini. (Method : POST)</div>
            <div>URL : {BASE_URL}/bed</div>
            <p>&nbsp;</p>
            <p>Body</p>
            <pre class="brush: ruby" name="code">
              {
                    "kapasitas_vip": "9",
                    "kapasitas_kelas_1": "9",
                    "kapasitas_kelas_2": "9",
                    "kapasitas_kelas_3": "9",
                    "kapasitas_hcu": "9",
                    "kapasitas_iccu": "9",
                    "kapasitas_icu_negatif_ventilator": "9",
                    "kapasitas_icu_negatif_tanpa_ventilator": "9",
                    "kapasitas_icu_tanpa_negatif_ventilator": "9",
                    "kapasitas_icu_tanpa_negatif_tanpa_ventilator": "9",
                    "kapasitas_isolasi_negatif": "9",
                    "kapasitas_isolasi_tanpa_negatif": "9",
                    "kapasitas_nicu_covid": "9",
                    "kapasitas_perina_covid": "9",
                    "kapasitas_picu_covid": "9",
                    "kapasitas_ok_covid": "9",
                    "kapasitas_hd_covid": "9",
                    "kosong_vip": "9",
                    "kosong_kelas_1": "9",
                    "kosong_kelas_2": "9",
                    "kosong_kelas_3": "9",
                    "kosong_hcu": "9",
                    "kosong_iccu": "9",
                    "kosong_icu_negatif_ventilator": "9",
                    "kosong_icu_negatif_tanpa_ventilator": "9",
                    "kosong_icu_tanpa_negatif_ventilator": "9",
                    "kosong_icu_tanpa_negatif_tanpa_ventilator": "9",
                    "kosong_isolasi_negatif": "9",
                    "kosong_isolasi_tanpa_negatif": "9",
                    "kosong_nicu_covid": "9",
                    "kosong_perina_covid": "9",
                    "kosong_picu_covid": "9",
                    "kosong_ok_covid": "9",
                    "kosong_hd_covid": "9"
              }
            </pre>
            <p>Keterangan</p>
            <div>kapasitas_vip = Jumlah Bed VIP (non-covid) yang dimiliki oleh Rumah Sakit</div>
            <div>kapasitas_kelas_1 = Jumlah Bed Kelas 1 (non-covid) yang dimiliki oleh Rumah Sakit</div>
            <div>kapasitas_kelas_2 = Jumlah Bed Kelas 2 (non-covid) yang dimiliki oleh Rumah Sakit</div>
            <div>kapasitas_kelas_3 = Jumlah Bed Kelas 3 (non-covid) yang dimiliki oleh Rumah Sakit</div>
            <div>kapasitas_hcu = Jumlah Bed HCU (non-covid) yang dimiliki oleh Rumah Sakit</div>
            <div>kapasitas_iccu = Jumlah Bed ICCU (non-covid) yang dimiliki oleh Rumah Sakit</div>
            <div>kapasitas_icu_negatif_ventilator = Jumlah Bed ICU Tekanan Negatif Dengan Ventilator yang dimiliki oleh Rumah Sakit</div>
            <div>kapasitas_icu_negatif_tanpa_ventilator = Jumlah Bed ICU Tekanan Negatif Tanpa Ventilator yang dimiliki oleh Rumah Sakit</div>
            <div>kapasitas_icu_tanpa_negatif_ventilator = Jumlah Bed ICU Tanpa Tekanan Negatif dengan Ventilator yang dimiliki oleh Rumah Sakit</div>
            <div>kapasitas_icu_tanpa_negatif_tanpa_ventilator = Jumlah Bed ICU Tanpa Tekanan Negatif tanpa Ventilator yang dimiliki oleh Rumah Sakit</div>
            <div>kapasitas_isolasi_negatif = Jumlah Bed Isolasi Tekanan Negatif yang dimiliki oleh Rumah Sakit</div>
            <div>kapasitas_isolasi_tanpa_negatif = Jumlah Bed Isolasi Tanpa Tekanan Negatif yang dimiliki oleh Rumah Sakit</div>
            <div>kapasitas_nicu_covid = Jumlah Bed NICU khusus COVID-19 yang dimiliki oleh Rumah Sakit</div>
            <div>kapasitas_perina_covid = Jumlah Bed Perina khusus COVID-19 yang dimiliki oleh Rumah Sakit</div>
            <div>kapasitas_picu_covid = Jumlah Bed PICU khusus COVID-19 yang dimiliki oleh Rumah Sakit</div>
            <div>kapasitas_ok_covid = Jumlah Bed OK khusus COVID-19 yang dimiliki oleh Rumah Sakit</div>
            <div>kapasitas_hd_covid = Jumlah Bed HD khusus COVID-19 yang dimiliki oleh Rumah Sakit</div>

            <br>

            <div>kosong_vip = Jumlah Bed VIP (non-covid) yang belum terisi di Rumah Sakit</div>
            <div>kosong_kelas_1 = Jumlah Bed Kelas 1 (non-covid) yang belum terisi di Rumah Sakit</div>
            <div>kosong_kelas_2 = Jumlah Bed Kelas 2 (non-covid) yang belum terisi di Rumah Sakit</div>
            <div>kosong_kelas_3 = Jumlah Bed Kelas 3 (non-covid) yang belum terisi di Rumah Sakit</div>
            <div>kosong_hcu = Jumlah Bed HCU (non-covid) yang belum terisi di Rumah Sakit</div>
            <div>kosong_iccu = Jumlah Bed ICCU (non-covid) yang belum terisi di Rumah Sakit</div>
            <div>kosong_icu_negatif_ventilator = Jumlah Bed ICU Tekanan Negatif Dengan Ventilator yang belum terisi di Rumah Sakit</div>
            <div>kosong_icu_negatif_tanpa_ventilator = Jumlah Bed ICU Tekanan Negatif Tanpa Ventilator yang belum terisi di Rumah Sakit</div>
            <div>kosong_icu_tanpa_negatif_ventilator = Jumlah Bed ICU Tanpa Tekanan Negatif dengan Ventilator yang belum terisi di Rumah Sakit</div>
            <div>kosong_icu_tanpa_negatif_tanpa_ventilator = Jumlah Bed ICU Tanpa Tekanan Negatif tanpa Ventilator yang belum terisi di Rumah Sakit</div>
            <div>kosong_isolasi_negatif = Jumlah Bed Isolasi Tekanan Negatif yang belum terisi di Rumah Sakit</div>
            <div>kosong_isolasi_tanpa_negatif = Jumlah Bed Isolasi Tanpa Tekanan Negatif yang belum terisi di Rumah Sakit</div>
            <div>kosong_nicu_covid = Jumlah Bed NICU khusus COVID-19 yang belum terisi di Rumah Sakit</div>
            <div>kosong_perina_covid = Jumlah Bed Perina khusus COVID-19 yang belum terisi di Rumah Sakit</div>
            <div>kosong_picu_covid = Jumlah Bed PICU khusus COVID-19 yang belum terisi di Rumah Sakit</div>
            <div>kosong_ok_covid = Jumlah Bed OK khusus COVID-19 yang belum terisi di Rumah Sakit</div>
            <div>kosong_hd_covid = Jumlah Bed HD khusus COVID-19 yang belum terisi di Rumah Sakit</div>

            <p>&nbsp;</p>
            <p>Response Success</p>
            <pre class="brush: ruby" name="code">
              {
                  "kode": 200,
                  "messages": "SUCCESS POST DATA BED"
              }
            </pre>
          </div>
        </div>

        <div class="resume-item d-flex flex-column flex-md-row justify-content-between mb-5">
          <div class="resume-content">
            <h3 class="mb-0">Get Data Ketersediaan Bed</h3>
            <div class="subheading mb-3">Digunakan untuk melihat data ketersediaan bed masing-masing rumah sakit (Method : GET)</div>
            <div>URL    : {BASE_URL}/bed</div>
            <p>&nbsp;</p>
            <p>Response Success</p>
            <pre class="brush: ruby" name="code">
              {
                  "kode": 200,
                  "messages": "SUCCESS GET DATA BED",
                  "response": {
                      "faskes": "RSUPN Dr. Cipto Mangunkusumo",
                      "kapasitas_vip": "9",
                      "kapasitas_kelas_1": "9",
                      "kapasitas_kelas_2": "9",
                      "kapasitas_kelas_3": "9",
                      "kapasitas_hcu": "9",
                      "kapasitas_iccu": "9",
                      "kapasitas_icu_negatif_ventilator": "9",
                      "kapasitas_icu_negatif_tanpa_ventilator": "9",
                      "kapasitas_icu_tanpa_negatif_ventilator": "9",
                      "kapasitas_icu_tanpa_negatif_tanpa_ventilator": "9",
                      "kapasitas_isolasi_negatif": "9",
                      "kapasitas_isolasi_tanpa_negatif": "9",
                      "kapasitas_nicu_covid": "9",
                      "kapasitas_perina_covid": "9",
                      "kapasitas_picu_covid": "9",
                      "kapasitas_ok_covid": "9",
                      "kapasitas_hd_covid": "9",
                      "kosong_vip": "9",
                      "kosong_kelas_1": "9",
                      "kosong_kelas_2": "9",
                      "kosong_kelas_3": "9",
                      "kosong_hcu": "9",
                      "kosong_iccu": "9",
                      "kosong_icu_negatif_ventilator": "9",
                      "kosong_icu_negatif_tanpa_ventilator": "9",
                      "kosong_icu_tanpa_negatif_ventilator": "9",
                      "kosong_icu_tanpa_negatif_tanpa_ventilator": "9",
                      "kosong_isolasi_negatif": "9",
                      "kosong_isolasi_tanpa_negatif": "9",
                      "kosong_nicu_covid": "9",
                      "kosong_perina_covid": "9",
                      "kosong_picu_covid": "9",
                      "kosong_ok_covid": "9",
                      "kosong_hd_covid": "9",
                      "updated_time": "2020-09-09 00:43:52"
                  }
              }
            </pre>
          </div>
        </div>

        <div class="resume-item d-flex flex-column flex-md-row justify-content-between mb-5">
          <div class="resume-content">
            <h3 class="mb-0">Delete Data Ketersediaan Bed</h3>
            <div class="subheading mb-3">Digunakan untuk menghapus data ketersediaan bed masing-masing rumah sakit (Method : DELETE)</div>
            <div>URL    : {BASE_URL}/bed</div>
            <p>&nbsp;</p>
            <p>Response</p>
            <pre class="brush: ruby" name="code">
              {
                  "kode": 200,
                  "messages": "SUCCESS DELETE DATA BED"
              }
            </pre>
          </div>
        </div>

        
      </div>
    </section>

    <hr class="m-0">

  </div>

  <!-- Bootstrap core JavaScript -->
  <script src="<?php echo base_url();?>assets/vendor/jquery/jquery.min.js"></script>
  <script src="<?php echo base_url();?>assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Plugin JavaScript -->
  <script src="<?php echo base_url();?>assets/vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for this template -->
  <script src="<?php echo base_url();?>assets/js/resume.min.js"></script>

</body>

</html>
