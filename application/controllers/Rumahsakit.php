<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . 'libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Rumahsakit extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        // $this->methods['kelurahan_get']['limit'] = 100; // 500 requests per hour per user/key
        // $this->methods['rw_get']['limit'] = 100; // 500 requests per hour per user/key
        // $this->methods['users_post']['limit'] = 100; // 100 requests per hour per user/key
        // $this->methods['users_delete']['limit'] = 50; // 50 requests per hour per user/key
        $this->load->model('Master_model');
    }

    public function index_get(){
        $puskesmas = $this->Master_model->list_rs();
        $jml = count($rumahsakit);
        $data = array();

        if ($jml > 0){

            foreach ($rumahsakit as $rs) {
                $in = array();
                $in['id_rs'] = $rs->id_rs;
                $in['nama_rumah_sakit'] = $rs->nama_rumah_sakit;
                $in['alamat_rumah_sakit'] = $rs->alamat_rumah_sakit;
                $in['kelurahan']['id_kelurahan'] = $rs->id_kelurahan;
                $in['kelurahan']['nama_kelurahan'] = $rs->nama_kelurahan;
                $in['kecamatan']['id_kecamatan'] = $pkm->id_kecamatan;
                $in['kecamatan']['nama_kecamatan'] = $pkm->nama_kecamatan;
                $in['kota']['id_kota'] = $rs->id_kota;
                $in['kota']['nama_kota'] = $rs->nama_kota;
                $in['kodepos'] = $rs->kodepos;
                $in['nomor_telepon'] = $rs->nomor_telepon;
                $in['nomor_fax'] = $rs->nomor_fax;
                $in['no_hp_direktur/kepala_rs'] = $rs->no_hp_direktur;
                $in['website'] = $rs->website;
                $in['email'] = $rs->email;

                $data[] = $in;
            }

            $this->set_response(
                array(
                    "status" => "success",
                    "code" => 200,
                    "count" => $jml,
                    "data" => $data
            ), REST_Controller::HTTP_OK); 
        } else {
            $this->set_response(
                array(
                    "status" => "Tidak ada data",
                    "kode" => 404,
                    "count" => 0,
                    "data" => array()
            ), REST_Controller::HTTP_NOT_FOUND); 
        }            
    }

    


    

}
