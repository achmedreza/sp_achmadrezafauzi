<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master_model extends CI_Model{

    function __construct() {
        parent::__construct();
    }

    public function add_log($data){
        $insert = $this->db->insert('t_log_user',$data);
        return $insert;
    }

    public function list_rs(){
        $subquery = "SELECT
            * 
        FROM
            m_rs
        LEFT JOIN m_kelurahan ON m_rs.kode_keluraha = m_kelurahan.id_kelurahan
        LEFT JOIN m_kecamatan ON m_rs.kode_kecamatan = m_kecamatan.id_kecamatan
        LEFT JOIN m_kota ON m_kecamatan.kode_kota = m_kota.id_kota
        LEFT JOIN m_provinsi ON m_kota.kode_provinsi = m_provinsi.id_provinsi
        ";
        
        $this->db->from('('.$subquery.') a');
        $query = $this->db->get();
        return $query->result();
    }

}