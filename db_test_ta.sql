/*
 Navicat Premium Data Transfer

 Source Server         : local
 Source Server Type    : MySQL
 Source Server Version : 100128
 Source Host           : localhost:3306
 Source Schema         : db_test_ta

 Target Server Type    : MySQL
 Target Server Version : 100128
 File Encoding         : 65001

 Date: 21/12/2021 16:18:24
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for m_kecamatan
-- ----------------------------
DROP TABLE IF EXISTS `m_kecamatan`;
CREATE TABLE `m_kecamatan`  (
  `id_kecamatan` varchar(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `nama_kecamatan` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `kode_kota` varchar(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  PRIMARY KEY (`id_kecamatan`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of m_kecamatan
-- ----------------------------
INSERT INTO `m_kecamatan` VALUES ('317101', 'Gambir', '3171');
INSERT INTO `m_kecamatan` VALUES ('317102', 'Tanah Abang', '3171');
INSERT INTO `m_kecamatan` VALUES ('317103', 'Menteng', '3171');
INSERT INTO `m_kecamatan` VALUES ('317105', 'Kemayoran', '3171');

-- ----------------------------
-- Table structure for m_kelurahan
-- ----------------------------
DROP TABLE IF EXISTS `m_kelurahan`;
CREATE TABLE `m_kelurahan`  (
  `id_kelurahan` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `nama_kelurahan` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `kode_kecamatan` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of m_kelurahan
-- ----------------------------
INSERT INTO `m_kelurahan` VALUES ('3171012', 'Cideng', '317101');
INSERT INTO `m_kelurahan` VALUES ('3171021', 'Bendungan Hilir', '317102');
INSERT INTO `m_kelurahan` VALUES ('3171031', 'Cikini', '317103');

-- ----------------------------
-- Table structure for m_kota
-- ----------------------------
DROP TABLE IF EXISTS `m_kota`;
CREATE TABLE `m_kota`  (
  `id_kota` varchar(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `nama_kota` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `kode_provinsi` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  PRIMARY KEY (`id_kota`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of m_kota
-- ----------------------------
INSERT INTO `m_kota` VALUES ('3171', 'Jakarta Pusat', '31');
INSERT INTO `m_kota` VALUES ('3172', 'Jakarta Timur', '31');
INSERT INTO `m_kota` VALUES ('3174', 'Jakarta Selatan', '31');

-- ----------------------------
-- Table structure for m_provinsi
-- ----------------------------
DROP TABLE IF EXISTS `m_provinsi`;
CREATE TABLE `m_provinsi`  (
  `id_provinsi` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `nama_provinsi` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of m_provinsi
-- ----------------------------
INSERT INTO `m_provinsi` VALUES ('31', 'DKI Jakarta');

-- ----------------------------
-- Table structure for m_rs
-- ----------------------------
DROP TABLE IF EXISTS `m_rs`;
CREATE TABLE `m_rs`  (
  `id_rs` varchar(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `nama_rumah_sakit` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `alamat_rumah_sakit` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `nomor_telepon` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `nomor_fax` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `no_hp_direktur` varchar(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `website` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `email` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `kode_kelurahan` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `kode_kecamatan` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `kode_kota` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `kodepos` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `keterangan` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `jenis_rumah_sakit` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  PRIMARY KEY (`id_rs`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of m_rs
-- ----------------------------
INSERT INTO `m_rs` VALUES ('317001', 'Tarakan', 'Jl. Kyai Caringin No. 7', '021-3503150, 021-3503003, 021-3508993', '021-3503412, 021-3863309', '0815 11445555', 'http://www.rstarakanjakarta.com', 'rsd_tarakan@yahoo.com', '3171012', '317101', '3171', '10150', 'Rujukan Covid-19', 'Rumah Sakit Umum Daerah');
INSERT INTO `m_rs` VALUES ('317002', 'DR. Mintoharjo', 'Jl. Bendungan Hilir No. 17', '021-5703081, 021-5703082, 021-5703083, 021-5703084, 021-5703085', '021-5711997', NULL, 'http://www.rsaldrmintohardjo.com', 'rsalmintohardjo@ymail.com', '3171021', '317102', '3171', '10210', 'Rujukan Covid-19', 'Rumah Sakit Umum TNI - Angkatan Laut');
INSERT INTO `m_rs` VALUES ('317003', 'PGI Cikini', 'Jl. Raden Saleh No. 40', '021-38997777', '021-31924663, 021-31908391', NULL, NULL, 'tedjowa@yahoo.com, mail@rscikini.com', '3171031', '317103', '3171', '10330', 'Non Covid-19', 'Rumah Sakit Umum');

SET FOREIGN_KEY_CHECKS = 1;
